using System.Collections;
using System.Collections.Generic;
using Fungus;
using UnityEngine;
using UnityEngine.UI;

public class Number : MonoBehaviour
{
    public int _score;
    public Text _scoreText;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _scoreText.text = _score.ToString();
    }

    public void PlusNumber()
    {
        if (_score <= 3 )
        {
            _score ++;
        }
    }
    
    public void MinusNumber()
    {
        if (_score >= 1 )
        {
            _score --;
        }
    }
}
