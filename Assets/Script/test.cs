using System.Collections;
using System.Collections.Generic;
using Doozy.Runtime.UIManager.Components;
using UnityEngine;
using UnityEngine.UI;

public class test : MonoBehaviour
{
    private int levelisunlocked;

    public UIButton button_PressUnLocked;
    public UIButton button_Unlock;
    
    
    
    void Start()
    {
        button_Unlock.interactable = false;
        button_Unlock.gameObject.SetActive(false);
        
    }
    

    // Update is called once per frame
    public void UnLocked()
    {
        if (button_PressUnLocked.isOn)
        {
            button_Unlock.interactable = true;
            button_Unlock.gameObject.SetActive(true);
        }
    }
}
