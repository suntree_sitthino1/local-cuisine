using Doozy.Runtime.Colors.Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace Sandbox
{

    public class TimesealeInfo : MonoBehaviour
    {
        public TextMeshProUGUI TextMesh;
        public Text Text;

        private void Reset()
        {
            TextMesh ??= GetComponent<TextMeshProUGUI>();
            Text ??= GetComponent<Text>();
        }

        private void Update()
        {
            TextMesh.text = $"Timescale: {Time.timeScale}";
            Text.text= $"Timescale: {Time.timeScale}";
        }
    }
}