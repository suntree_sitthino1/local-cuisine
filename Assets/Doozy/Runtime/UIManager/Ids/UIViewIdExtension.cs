// Copyright (c) 2015 - 2022 Doozy Entertainment. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

//.........................
//.....Generated Class.....
//.........................
//.......Do not edit.......
//.........................

using System.Collections.Generic;
// ReSharper disable All
namespace Doozy.Runtime.UIManager.Containers
{
    public partial class UIView
    {
        public static IEnumerable<UIView> GetViews(UIViewId.LocalCuisine id) => GetViews(nameof(UIViewId.LocalCuisine), id.ToString());
        public static void Show(UIViewId.LocalCuisine id, bool instant = false) => Show(nameof(UIViewId.LocalCuisine), id.ToString(), instant);
        public static void Hide(UIViewId.LocalCuisine id, bool instant = false) => Hide(nameof(UIViewId.LocalCuisine), id.ToString(), instant);

        public static IEnumerable<UIView> GetViews(UIViewId.LocalCuisineAR id) => GetViews(nameof(UIViewId.LocalCuisineAR), id.ToString());
        public static void Show(UIViewId.LocalCuisineAR id, bool instant = false) => Show(nameof(UIViewId.LocalCuisineAR), id.ToString(), instant);
        public static void Hide(UIViewId.LocalCuisineAR id, bool instant = false) => Hide(nameof(UIViewId.LocalCuisineAR), id.ToString(), instant);

        public static IEnumerable<UIView> GetViews(UIViewId.Other id) => GetViews(nameof(UIViewId.Other), id.ToString());
        public static void Show(UIViewId.Other id, bool instant = false) => Show(nameof(UIViewId.Other), id.ToString(), instant);
        public static void Hide(UIViewId.Other id, bool instant = false) => Hide(nameof(UIViewId.Other), id.ToString(), instant);
    }
}

namespace Doozy.Runtime.UIManager
{
    public partial class UIViewId
    {
        public enum LocalCuisine
        {
            Boiling,
            Chooping,
            Choose,
            FinishPlate,
            Home,
            Load,
            Loaded,
            Pounding,
            RecipeStart,
            ScoreBar,
            ScorePanel
        }

        public enum LocalCuisineAR
        {
            AR
        }

        public enum Other
        {
            CookBookHowto,
            CookBookIngre,
            IngredientBar
        }    
    }
}