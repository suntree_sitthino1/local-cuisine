                           !\               0.0.0 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙   Ŕ           1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               \     ˙˙˙˙               H r   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                     Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                        \       ŕyŻ     `       ü                                                                                                                                                ŕyŻ                                                                                    MetaModyAction    // Copyright (c) 2015 - 2022 Doozy Entertainment. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System;
using Doozy.Runtime.Signals;
using UnityEngine;
using UnityEngine.Events;

namespace Doozy.Runtime.Mody
{
    /// <summary>
    /// Base class for actions, that have a value, in the Mody System.
    /// <para/> Any action with a value that interacts with the system needs to derive from this.
    /// <para/> It is used to start, stop and finish Module's (MonoBehaviour's) tasks.
    /// <para/> It can be triggered manually (via code) or by any Trigger registered to the system.
    /// </summary>
    /// <typeparam name="T"> Value type </typeparam>
    [Serializable]
    public abstract class MetaModyAction<T> : ModyAction
    {
        /// <summary> ModyAction's value </summary>
        public T ActionValue;

        /// <summary> Callback triggered by this ModyAction </summary>
        public UnityAction<T> actionCallback { get; private set; }

        /// <summary> Construct a new <see cref="MetaModyAction{T}"/> with the given settings </summary>
        /// <param name="behaviour"> MonoBehaviour the ModyAction belongs to </param>
        /// <param name="actionName"> Name of the action </param>
        /// <param name="callback"> Callback triggered by the ModyAction </param>
        protected MetaModyAction(MonoBehaviour behaviour, string actionName, UnityAction<T> callback) : base(behaviour, actionName)
        {
            actionCallback = callback;
            HasValue = true;
            ValueType = typeof(T);

            IgnoreSignalValue = false;
            ReactToAnySignal = false;
        }

        protected override void Run(Signal signal)
        {
            if (ReactToAnySignal)
            {
                if (IgnoreSignalValue)
                {
                    actionCallback?.Invoke(ActionValue); //invoke callbacks
                    return;
                }

                //do not react to any signal --> check for valid signal to update the value
                if (signal != null && signal.valueType == ValueType)
                    ActionValue = signal.GetValueUnsafe<T>(); //get value from signal

                actionCallback?.Invoke(ActionValue); //invoke callbacks
                return;
            }

            //do not react to any signal --> check for valid signal
            if (signal == null) return; //signal is null --> return
            if (!signal.hasValue) return; //signal does not have value --> return
            if (signal.valueType != ValueType) return; //signal value type does not match the action value type --> return

            if (IgnoreSignalValue)
            {
                actionCallback?.Invoke(ActionValue); //invoke callbacks
                return;
            }

            ActionValue = signal.GetValueUnsafe<T>(); //get value from signal
            actionCallback?.Invoke(ActionValue); //invoke callbacks
        }

        /// <summary> Set a new value to the ModyAction </summary>
        /// <param name="value"> New value </param>
        public void SetValue(T value) =>
            ActionValue = value;

        /// <summary> Set a new value to the ModyAction as an object </summary>
        /// <param name="objectValue"> New value as an object </param>
        /// <returns> True if the operation was a success and false otherwise </returns>
        public override bool SetValue(object objectValue) => 
            SetValue(objectValue, true);
        
        internal override bool SetValue(object objectValue, bool restrictValueType)
        {
            if (objectValue == null) return false;
            if (restrictValueType && objectValue.GetType() != ValueType)
                return false;
            try
            {
                SetValue((T)objectValue);
                return true;
            }
            catch
            {
                // ignored
                return false;
            }
        }
    }
}
                       MetaModyAction      